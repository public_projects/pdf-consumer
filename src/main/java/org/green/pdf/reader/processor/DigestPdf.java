package org.green.pdf.reader.processor;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import java.io.File;

@Service
public class DigestPdf implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        File pdfFile = new File("D:\\Users\\sm13\\Documents\\development\\pdf-consumer\\top-up-50.pdf");
        PDDocument document = PDDocument.load(pdfFile);
        PDFTextStripper pdfStripper = new PDFTextStripper();
        String text = pdfStripper.getText(document);
        System.err.println("text"+text);
    }
}
